---
title: "Video1_GraphNeuralNetworks"
format: html
editor: visual
jupyter: python3
# engine: knitr
date: "`r format(Sys.time(), '%d %B, %Y')`"
author: 
  - name: massisenergy
    id: jc
    orcid: 0000-0000-0000-0000
    email: 
    affiliation: 
      - name: University
        city: Providence
        state: RI
        url: www.ac.edu
abstract: > 
  Introduction to sequence, quality, control, 
  mapping, and variant calling.
keywords:
  - MacOS 15
  - zsh
  - arm64
  - python 3
license: "CC BY"
copyright: 
  holder: massisenergy
  year: 2024
citation: 
  container-title: none
  volume: 0
  issue: 0
  doi: 10.5555/12345678
funding: "The author received no specific funding for this work."
---

# Stanford CS224W: Machine Learning w/ Graphs \| 2023 [Graph Neural Networks](https://www.youtube.com/watch?v=ZfK4FDk9uy8&list=PLhlnCXGrJEEF50wrqIf42N4QIFuTe7g1V)

## Node embeddings:

### Shallow encoding limitations:

-   **no parameter sharing** between nodes leading to large number of parameters
-   ***transductive***: cannot generate embeddings for nodes not seen during training)
-   **no features in nodes**

## Goals to achieve with these networks

 - **Node classification**: Predict the type of a given node (drug toxicity detection)
 - **Link prediction**: Predict whether two nodes are linked (user recommendation system)
 - **Community detection**: Identify densely linked clusters of nodes (detection anomaly in financial transactions)
 - **Network similarity**: How similar are two (sub)networks (classify molecules)
 
## Deep Learning Basics
### ML as Optimization problem
find optimal parameters to minimize the objective function:

$min\Theta \mathcal{L}(y, f(x))$

$\Theta={Z}$ in the *shallow encoder*. We can use different types of loss function (L1, L2, cross-entropy, huber, hinge, etc.), depending on the problem. E.g. L2 loss:

$\mathcal{L}(y, f(x))=||y-f(x)||_2$

### MLP combines linear transformation and non-linearity

$x^{(l+1)}=\sigma(W_lx^l+b^l)$

where
$W_l$, is weight matrix that transforms hidden representation at layer $l$ to layer $l + 1$

$b^l$ is bias at layer $l$, and is added to the linear transformation of $x^l$)

$\sigma$ is non-linearity function (e.g., sigmoid) 
 
* f can be simple linear layer, MLP, GNN, etc.
* sample a minibatch of input x
* Forward propagation: GIven x, compute $\mathcal{L}$
* back propagation: obtain gradient $\nabla_w\mathcal{L}$ using a chain rule.
* Stochastic gradient decent (SGD) to optimize $\Theta$ over interations

### Deep Learning for graphs
Description:

* Local network neighborhoods
    + aggregation strategies
    + computation graphs
* Stacking layers
    + model, parameters, training
    + model fitting
    + unsupervised and supervised training example

#### Defining a graph G:
* $V$ is the vertex set
* $A$ is the adjacency matrix (assume binary)
* $X \in \mathbb{R}|V|\times d$ is a matrix of node features
* $v$: a node in $V$; $N(v)$: the set of neighbors of v.
* Node Features
    + Social networks: User profile, User image
    + Biological networks: Gene expression profiles, gene functional information
    + When there is no node feature in the graph dataset:
        - Indicator vectors (one-hot encoding of a node)
        - Vector of constant 1: [1, 1, .., 1]

##### **Approach 1**: Simple MLP
join adjacency matrix with node features and feed into a deep NN. Limitations:

* $0(|V|\times d)$ parameters
* Not applicable to graphs of different sizes
* Sensitive to node ordering

##### **Approach 2**: CNN as graph. 
Limitations:

* no fixed sliding window or locality for general graphs.
* graph permutation invariant (no canonical node-ordering), but the CNN filter will treat it differently if the nodes are shuffled.

##### **Approach 3**: Graphs
1. **Permutation invariance**
For any graph function $f\colon\mathbb{R}^{|V|\times m}\times\mathbb{R}^{|V|\times|V|}\to\mathbb{R}^d$, f is permutation-invariant if for any permutation: $f(A,X)=f(PAP^T,PX)$ holds. Example:
$f(A,X)=1^TX because, F(PAP^T,PX)=1^TPX=


![](/Users/massisenergy/Library/CloudStorage/OneDrive-MSFT/Coding_oNLYiNsHAREPOINT/Python/venv_torch-gpu/Python_R/CS224W_MLwithGraphs_2021-2023_aka_GraphNeuralNetworks/Data/GraphPermetationInvariance.png)

2. **Permutation equivariance**
If the output vector of a node at the same position in the graph remains unchanged for any order plan, we say f is permutation equivariant.
$Pf(A,X)=f(PAP^T,PX)$ Example: 
* $f(A,X)=X$ (shallow encoder)
* $f(A,X)=AX$ ()

Naïve MLP doesn't satisfy permutation invariance or equivariance, so it fails for graphs.

### Design graph neutral network for permutation invariance/equivalence by passing and aggregating information from neighbors
##### graph convolutional networks








```{python}
1 + 1
```

You can add options to executable code like this

```{python}
#| echo: false
2 * 2
```

The `echo: false` option disables the printing of code (only output is displayed).
