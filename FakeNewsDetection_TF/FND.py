import pandas as pd
# from google.colab import output
from collections import Counter
import numpy as np

# K-Mean
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans

import tensorflow as tf
from tensorflow.keras.layers import Embedding
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import Sequential
from tensorflow.keras.preprocessing.text import one_hot
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Bidirectional
from tensorflow.keras.layers import Dropout

import nltk
import re
from nltk.corpus import stopwords

from nltk.stem.porter import PorterStemmer

from sklearn.model_selection import train_test_split

from sklearn.metrics import confusion_matrix

from sklearn.metrics import accuracy_score

from sklearn.metrics import classification_report

import warnings
from sklearn.metrics import precision_recall_fscore_support
import pickle
import openpyxl

sheetUrl = "https://docs.google.com/spreadsheets/d/1azAfOVkkz42F3V9e0P9N5LHD4YbO5P--CFjIKBIxbEc/edit?usp=sharing"
sheetId = max(sheetUrl.split('/'), key=len)
sheetName = 'Train'
sheetUrl = "https://docs.google.com/spreadsheets/d/{}/gviz/tq?tqx=out:csv&sheet={}".format(sheetId, sheetName)
spreadsheet = pd.read_csv(sheetUrl)
spreadsheet = spreadsheet.dropna()
spreadsheet = spreadsheet.reset_index(drop=True)

new_sheet = pd.DataFrame(columns=["id",	"title",	"author",	"text",	"label"])
def writetofile():
  global new_sheet
  with pd.ExcelWriter('NewDataSheet.xlsx', engine='openpyxl') as writer:
    new_sheet.to_excel(writer, sheet_name='Sheet1', index=False)


class Models:
    def __init__(self, grouped_sheet):
        global new_sheet
        store_sheet = grouped_sheet
        independent_feature = grouped_sheet.drop('label', axis=1)
        dependent_feature = grouped_sheet['label']
        voc_size = 100000
        independent_feature.reset_index(inplace=True)
        nltk.download('stopwords')
        ps = PorterStemmer()
        corpus = []
        for i in range(0, len(independent_feature)):
            print(i)
            review = re.sub('[^a-zA-Z]', ' ', independent_feature['title'][i])
            review = review.lower()
            review = review.split()

            review = [ps.stem(word) for word in review if not word in stopwords.words('english')]
            review = ' '.join(review)
            corpus.append(review)
        independent_feature = [one_hot(words, voc_size) for words in corpus]
        independent_feature[1]
        sent_length = 100
        embedded_docs = pad_sequences(independent_feature, padding='pre', maxlen=sent_length)
        embedding_vector_features = 40
        model = Sequential()
        model.add(Embedding(voc_size, embedding_vector_features, input_length=sent_length))
        model.add(Bidirectional(LSTM(100)))
        model.add(Dropout(0.3))
        model.add(Dense(1, activation='sigmoid'))
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

        independent_feature_train = np.array(embedded_docs[:-1])
        dependent_feature_train = np.array(dependent_feature.iloc[:-1])
        independent_feature_test = np.array(embedded_docs[-1:])
        dependent_feature_test = np.array([dependent_feature.iloc[-1]])
        independent_feature_train.shape, dependent_feature_train.shape
        X_final = np.array(embedded_docs)
        y_final = np.array(dependent_feature)
        independent_feature_train, independent_feature_test, dependent_feature_train, dependent_feature_test = train_test_split(
            X_final, y_final, test_size=0.33, random_state=42)
        model.fit(independent_feature_train, dependent_feature_train,
                  validation_data=(independent_feature_test, dependent_feature_test), epochs=40, batch_size=64)
        predict_y = model.predict(independent_feature_test)
        y_pred1 = np.argmax(predict_y, axis=1)
        self.accuracy = accuracy_score(dependent_feature_test, y_pred1)
        if self.accuracy >= 80:
            store_sheet['id'] = 0
            new_sheet = pd.concat([new_sheet, store_sheet]).drop_duplicates().reset_index(drop=True)

    def result(self):
        return self.accuracy


class FNDSystem:
    def __init__(self, sheet, clusters):
        self.sheet = sheet
        self.clusters = clusters

    def kmean_clusterer(self):
        paragraph_list = self.sheet['text'].tolist()
        vectorizer = TfidfVectorizer(stop_words='english')
        vectored_data = vectorizer.fit_transform(paragraph_list)
        kmeans = KMeans(n_clusters=self.clusters, random_state=0).fit(vectored_data)
        count_dict = Counter(kmeans.labels_)
        # print(kmeans.labels_, count_dict)
        return self.grouper(kmeans.labels_)

    def grouper(self, labels):
        result_list = []
        for index, label in enumerate(labels):
            self.sheet.loc[index, 'id'] = label
        self.sheet = self.sheet.groupby('id')
        for index in range(self.clusters):
            size = self.sheet.get_group(list(self.sheet.groups.keys())[index]).shape[0]
            model_object = Models(self.sheet.get_group(list(self.sheet.groups.keys())[index]))
            result_list.append({"size": size, "accuracy": model_object.result()})
        return result_list

import openpyxl
workbook = openpyxl.Workbook()
worksheet = workbook.active
header = [i for i in range(1, 21)]
worksheet.append(['Partition'] + header)
workbook.save('Accuracy.xlsx')

def write(turn, row_list):
  workbook = openpyxl.load_workbook('Accuracy.xlsx')
  worksheet = workbook.active
  row_size = []
  row_accuracy = []
  for item in row_list:
    row_size.append(item["size"])
    row_accuracy.append(item["accuracy"])
  worksheet.append([turn] + row_size)
  worksheet.append(["Accuracy"] + row_accuracy)
  workbook.save('Accuracy.xlsx')

for turn in range(1, 21):
  FND_object = FNDSystem(spreadsheet, turn)
  result_list = FND_object.kmean_clusterer()
  write(turn, result_list)
writetofile()
file_path = 'NewDataSheet.xlsx'
newSheet = pd.read_excel(file_path)
final_model = Models(newSheet)
write("Final", [{"size": newSheet.shape[0], "accuracy": final_model.result}])