{
 "cells": [
  {
   "cell_type": "markdown",
   "source": [
    "# Utilizing `pdb`, mock` & `unittest` to test neural network models.\n",
    "- `mock` is designed for use with `unittest` and is based on the\n",
    "‘action -> assertion’ pattern instead of ‘record -> replay’ used by many\n",
    "mocking frameworks.\n",
    "- [The Mock Class](https://docs.python.org/3/library/unittest.mock.html#the-mock-class)\n",
    "- Problem Definition: The user has to guess a [MASK] with the suggested words and provided sentence.\n",
    "- Loading & initializing pretrained model & tokenizer takes time. Using the `Mock` functions, we have the following\n",
    "    - *advantage:*\n",
    "        - **faster:** the mocking of the model and tokenizer is nearly instantaneous, compared to loading huge models in some cases.\n",
    "        - **no download:** downloading the model and tokenizer is not required.\n",
    "    - *disadvantage:*\n",
    "        - **different beahivior** of the real & mocked objects can lead to different results.\n",
    "        - **complicated:** mocking is not trivial and requires some knowledge of the codebase, compared to testing with the real object.\n"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting application.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile application.py\n",
    "import logging\n",
    "import sys\n",
    "\n",
    "import numpy as np\n",
    "import torch\n",
    "from transformers import AutoTokenizer, AutoModelForMaskedLM\n",
    "\n",
    "def get_top_k(sequence, tokenizer, model, k=10):\n",
    "    \"\"\"Get top (most probable) k words from the model for the masked sequence.\n",
    "    Args:\n",
    "    --------\n",
    "        sequence (str): The masked sequence.\n",
    "        tokenizer (transformers.PreTrainedTokenizer): The tokenizer.\n",
    "        model (transformers.PreTrainedModel): The model.\n",
    "        k (int, optional): The number of top words to return. Defaults to 10.\n",
    "\n",
    "    Returns:\n",
    "    -------\n",
    "       top_indices (torch.tensor): 1D tensor representing the top indices\n",
    "    \"\"\"\n",
    "    batch_encoding=tokenizer(sequence, return_tensors=\"pt\")\n",
    "    mask_idx=torch.where(batch_encoding[\"input_ids\"]==tokenizer.mask_token_id\n",
    "                           )[1]\n",
    "    logits=model(**batch_encoding).logits\n",
    "    top_vocab_indices=torch.topk(logits[0, mask_idx.item(), :], k)[1]\n",
    "    return top_vocab_indices\n",
    "\n",
    "if __name__==\"__main__\":\n",
    "    logging.disable(logging.WARNING)\n",
    "    tokenizer=AutoTokenizer.from_pretrained(\"bert-base-uncased\")\n",
    "    model=AutoModelForMaskedLM.from_pretrained(\"bert-base-uncased\")\n",
    "    sequence=sys.argv[1]\n",
    "    top_vocab_indices=get_top_k(sequence, tokenizer, model, 5)\n",
    "    top_tokens=[tokenizer.decode(torch.tensor[idx]) for idx in\n",
    "                top_vocab_indices]\n",
    "    correct_token=top_tokens[0]\n",
    "    print(np.random.permutation(top_tokens))\n",
    "    guess=input(\"Guess the masked token: \").strip()\n",
    "\n",
    "    if guess==correct_token:\n",
    "        print(\"Correct!\")\n",
    "    else:\n",
    "        print(f\"Correct token is: {correct_token}\")\n",
    "\n",
    "    for i, token in enumerate(top_tokens):\n",
    "        print(f\"{i+1}. {token}\")"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "start_time": "2023-04-05T19:36:30.348070Z",
     "end_time": "2023-04-05T19:36:30.354183Z"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "The testing works fine, but the `command time` command shows that the **real time** is between 7-11 seconds!\n",
    "```zsh\n",
    "python application.py \"I wish I [MASK] more when I was younger.\"\n",
    "command time python application.py \"I wish I [MASK] more when I was younger.\" -h\n",
    "```\n",
    "This is because instantiating the model and tokenizer takes a lot of time.\n",
    "So, now we will use `mock` to *unittest* the `get_top_k` function."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting test_application.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile test_application.py\n",
    "import pytest\n",
    "import torch\n",
    "from transformers import AutoTokenizer, AutoModelForMaskedLM\n",
    "\n",
    "from application import get_top_k\n",
    "\n",
    "@pytest.mark.parametrize(\"k\", [5, 7, 10]) # test with different k\n",
    "def test_with_real_objects(k):\n",
    "    tokenizer=AutoTokenizer.from_pretrained(\"bert-base-uncased\")\n",
    "    model=AutoModelForMaskedLM.from_pretrained(\"bert-base-uncased\")\n",
    "    # we want to just assert if the output is a torch.Tensor and\n",
    "    # it has the correct shape\n",
    "    sequence=\"HELLO [MASK]\"\n",
    "    res=get_top_k(sequence, tokenizer, model, k)\n",
    "    assert isinstance(res, torch.Tensor)\n",
    "    assert res.shape==(k,)"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "start_time": "2023-04-05T18:13:58.471157Z",
     "end_time": "2023-04-05T18:13:58.475095Z"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "```zsh\n",
    "pytest --durations=5 -vv test_application.py\n",
    "```\n",
    "Shows that the function `test_with_real_objects` **call** takes 4.7 seconds. So, we would like to write a second test without using the real objects  `tokenizer` and `model`.\n",
    "Before that we will use `breakpoint` & **pdb** to see what is happening inside the function.\n",
    "- `breakpoint()` is a function that will stop the execution of the program and drop you into the debugger.\n",
    "- `pdb` is a python debugger.\n",
    "- `l` is used to list the code around the current line.\n",
    "- 'type(<object>)` is used to check the type of an object.\n",
    "- `n` is used to go to the next line.\n",
    "- `tokenizer.mask_token_id` shows what it's returning."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting application_with_breakpoint.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile application_with_breakpoint.py\n",
    "import logging\n",
    "import sys\n",
    "\n",
    "import numpy as np\n",
    "import torch\n",
    "from transformers import AutoTokenizer, AutoModelForMaskedLM\n",
    "\n",
    "def get_top_k(sequence, tokenizer, model, k=10):\n",
    "    \"\"\"Get top (most probable) k words from the model for the masked sequence.\n",
    "    Args:\n",
    "    --------\n",
    "        sequence (str): The masked sequence.\n",
    "        tokenizer (transformers.PreTrainedTokenizer): The tokenizer.\n",
    "        model (transformers.PreTrainedModel): The model.\n",
    "        k (int, optional): The number of top words to return. Defaults to 10.\n",
    "\n",
    "    Returns:\n",
    "    -------\n",
    "       top_indices (torch.tensor): 1D tensor representing the top indices\n",
    "    \"\"\"\n",
    "    breakpoint()\n",
    "    batch_encoding = tokenizer(sequence, return_tensors=\"pt\")\n",
    "    mask_idx = torch.where(batch_encoding[\"input_ids\"]==tokenizer.mask_token_id\n",
    "                           )[1]\n",
    "    logits = model(**batch_encoding).logits\n",
    "    top_vocab_indices = torch.topk(logits[0, mask_idx, :], k)[1]\n",
    "    return top_vocab_indices\n",
    "\n",
    "if __name__==\"__main__\":\n",
    "    logging.disable(logging.WARNING)\n",
    "    tokenizer=AutoTokenizer.from_pretrained(\"bert-base-uncased\")\n",
    "    model=AutoModelForMaskedLM.from_pretrained(\"bert-base-uncased\")\n",
    "    sequence=sys.argv[1]\n",
    "    top_vocab_indices=get_top_k(sequence, tokenizer, model)\n",
    "    top_tokens=[tokenizer.decode([idx]) for idx in top_vocab_indices]\n",
    "    correct_token=top_tokens[0]\n",
    "    print(np.random.permutation(top_tokens))\n",
    "    guess=input(\"Guess the masked token: \").strip()\n",
    "\n",
    "    if guess==correct_token:\n",
    "        print(\"Correct!\")\n",
    "    else:\n",
    "        print(f\"Correct token is: {correct_token}\")\n",
    "\n",
    "    for i, token in enumerate(top_tokens):\n",
    "        print(f\"{i+1}. {token}\")"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "start_time": "2023-04-05T19:26:18.420918Z",
     "end_time": "2023-04-05T19:26:18.424844Z"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "```zsh\n",
    "python application_with_breakpoint.py \"I wish I [MASK] more when I was younger.\"\n",
    "```"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "Now we have all the required information to write the test. We will use `mock` to mock the `tokenizer` and `model` objects."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting test_application_mock.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile test_application_mock.py\n",
    "from unittest.mock import Mock\n",
    "import pytest\n",
    "import torch\n",
    "from transformers import (AutoTokenizer, AutoModelForMaskedLM, BatchEncoding,\n",
    "                            BertTokenizerFast, BertForMaskedLM)\n",
    "\n",
    "from application import get_top_k\n",
    "\n",
    "@pytest.mark.parametrize(\"k\", [5, 7, 10]) # test with different k\n",
    "def test_with_real_objects(k):\n",
    "    tokenizer=AutoTokenizer.from_pretrained(\"bert-base-uncased\")\n",
    "    model=AutoModelForMaskedLM.from_pretrained(\"bert-base-uncased\")\n",
    "    # we want to just assert if the output is a torch.Tensor and\n",
    "    # it has the correct shape\n",
    "    sequence=\"HELLO [MASK]\"\n",
    "    res=get_top_k(sequence, tokenizer, model, k)\n",
    "    assert isinstance(res, torch.Tensor)\n",
    "    assert res.shape==(k,)\n",
    "\n",
    "@pytest.mark.parametrize(\"k\", [5, 7, 10])\n",
    "def test_with_mock_objects(k):\n",
    "    sequence_m=\"HELLO [MASK]\"\n",
    "    vocab_size=1000\n",
    "    data={\"input_ids\": torch.tensor([[101, 589, 973, 642]])}\n",
    "    be=BatchEncoding(data=data)\n",
    "    logits=torch.rand(1, 4, vocab_size)\n",
    "    # we will mock the tokenizer and model objects\n",
    "    tokenizer_m=Mock(spec=BertTokenizerFast,return_value=be,mask_token_id=973)\n",
    "    model_m=Mock(Model=BertForMaskedLM)\n",
    "    model_m.return_value.logits=logits\n",
    "    res=get_top_k(sequence_m, tokenizer_m, model_m, k=k)\n",
    "    assert isinstance(res, torch.Tensor)\n",
    "    assert res.shape==(k,)"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "start_time": "2023-04-06T21:37:48.929853Z",
     "end_time": "2023-04-06T21:37:48.934514Z"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "```zsh\n",
    "pytest --durations=5 -vv test_application_mock.py\n",
    "```"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [],
   "metadata": {
    "collapsed": false
   }
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
