import logging
import sys

import numpy as np
import torch
from transformers import AutoTokenizer, AutoModelForMaskedLM

def get_top_k(sequence, tokenizer, model, k=10):
    """Get top (most probable) k words from the model for the masked sequence.
    Args:
    --------
        sequence (str): The masked sequence.
        tokenizer (transformers.PreTrainedTokenizer): The tokenizer.
        model (transformers.PreTrainedModel): The model.
        k (int, optional): The number of top words to return. Defaults to 10.

    Returns:
    -------
       top_indices (torch.tensor): 1D tensor representing the top indices
    """
    batch_encoding=tokenizer(sequence, return_tensors="pt")
    mask_idx=torch.where(batch_encoding["input_ids"]==tokenizer.mask_token_id
                           )[1]
    logits=model(**batch_encoding).logits
    top_vocab_indices=torch.topk(logits[0, mask_idx.item(), :], k)[1]
    return top_vocab_indices

if __name__=="__main__":
    logging.disable(logging.WARNING)
    tokenizer=AutoTokenizer.from_pretrained("bert-base-uncased")
    model=AutoModelForMaskedLM.from_pretrained("bert-base-uncased")
    sequence=sys.argv[1]
    top_vocab_indices=get_top_k(sequence, tokenizer, model, 5)
    top_tokens=[tokenizer.decode(torch.tensor[idx]) for idx in 
                top_vocab_indices]
    correct_token=top_tokens[0]
    print(np.random.permutation(top_tokens))
    guess=input("Guess the masked token: ").strip()

    if guess==correct_token:
        print("Correct!")
    else:
        print(f"Correct token is: {correct_token}")

    for i, token in enumerate(top_tokens):
        print(f"{i+1}. {token}")
