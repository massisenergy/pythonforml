import pytest
import torch
from transformers import AutoTokenizer, AutoModelForMaskedLM

from application import get_top_k

@pytest.mark.parametrize("k", [5, 7, 10]) # test with different k
def test_with_real_objects(k):
    tokenizer=AutoTokenizer.from_pretrained("bert-base-uncased")
    model=AutoModelForMaskedLM.from_pretrained("bert-base-uncased")
    # we want to just assert if the output is a torch.Tensor and
    # it has the correct shape
    sequence="HELLO [MASK]"
    res=get_top_k(sequence, tokenizer, model, k)
    assert isinstance(res, torch.Tensor)
    assert res.shape==(k,)
