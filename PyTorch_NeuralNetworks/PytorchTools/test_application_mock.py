from unittest.mock import Mock
import pytest
import torch
from transformers import (AutoTokenizer, AutoModelForMaskedLM, BatchEncoding,
                            BertTokenizerFast, BertForMaskedLM)

from application import get_top_k

@pytest.mark.parametrize("k", [5, 7, 10]) # test with different k
def test_with_real_objects(k):
    tokenizer=AutoTokenizer.from_pretrained("bert-base-uncased")
    model=AutoModelForMaskedLM.from_pretrained("bert-base-uncased")
    # we want to just assert if the output is a torch.Tensor and
    # it has the correct shape
    sequence="HELLO [MASK]"
    res=get_top_k(sequence, tokenizer, model, k)
    assert isinstance(res, torch.Tensor)
    assert res.shape==(k,)

@pytest.mark.parametrize("k", [5, 7, 10])
def test_with_mock_objects(k):
    sequence_m="HELLO [MASK]"
    vocab_size=1000
    data={"input_ids": torch.tensor([[101, 589, 973, 642]])}
    be=BatchEncoding(data=data)
    logits=torch.rand(1, 4, vocab_size)
    # we will mock the tokenizer and model objects
    tokenizer_m=Mock(spec=BertTokenizerFast,return_value=be,mask_token_id=973)
    model_m=Mock(Model=BertForMaskedLM)
    model_m.return_value.logits=logits
    res=get_top_k(sequence_m, tokenizer_m, model_m, k=k)
    assert isinstance(res, torch.Tensor)
    assert res.shape==(k,)
