import pathlib
import torch
import torch.nn.functional as F
from torch.nn import Linear, Module
from torch.utils.tensorboard import SummaryWriter

class Network(Module):
    def __init__(self):
        super(Network, self).__init__()
        self.fc1=Linear(10, 100)
        self.fc2=Linear(100, 1000)
        self.fc3=Linear(1000, 2)

    def forward(self, x):
        x = self.fc1(x)
        x = self.fc2(x)
        x = self.fc3(x)
        x = F.relu(x)
        return x

if __name__ == "__main__":
    log_dir = pathlib.Path.cwd()/"../../tensorboard/visualize_activations_forwardHooks"
    writer = SummaryWriter(log_dir=log_dir)
    x = torch.randn(1, 10)
    net = Network()

    def activation_hook(module, input, output):
        """Run activation hook

        Parameters
        ----------
        module : torch.nn.Module
            Module
        input : torch.Tensor
            Input tensor to the `forward` method
        output : torch.Tensor
            Output tensor from the `forward` method
            the output can also be modified using the forward hook
        """
        print("Here")
        writer.add_histogram(repr(module), output)

    # register the forward hook just before calling the forward method
    handle_1=net.fc1.register_forward_hook(activation_hook)
    handle_2=net.fc2.register_forward_hook(activation_hook)
    handle_3=net.fc3.register_forward_hook(activation_hook)
    y=net(x) # forward pass
    # handle_1.remove() #removes the hook, comment out when not debugging
    # handle_2.remove()
    # handle_3.remove()
    print(y)
