import pathlib
import torch
import torch.nn.functional as F
from torch.nn import Linear, Module
from torch.utils.tensorboard import SummaryWriter

class Network(Module):
    def __init__(self):
        super(Network, self).__init__()
        self.fc1=Linear(10, 15)
        self.fc2=Linear(15, 20)
        self.fc3=Linear(20, 3)

        log_dir = pathlib.Path.cwd()/"../../tensorboard/visualize_activations_manually"
        self.writer = SummaryWriter(log_dir=log_dir)


    def forward(self, x):
        x = self.fc1(x)
        self.writer.add_histogram("fc1", x, 0)
        x = self.fc2(x)
        self.writer.add_histogram("fc2", x, 0)
        x = self.fc3(x)
        self.writer.add_histogram("fc3", x, 0)
        x = F.relu(x)
        return x

if __name__ == "__main__":
    net = Network()
    x = torch.randn(1, 10)
    y = net(x)
    print(y)
