import torch
import torch.nn as nn


class PatchEmbed(nn.Module):
    """
    1. Split the image into patches
    2. Linearly project the patches
    3. Flatten the projected patches into a single vector (embeddings)

    Parameters
    ----------
    img_size (assuming square) : int
        Size of the input image
    patch_size (assuming square) : int
        Size of the patches to be extracted from the input image
    in_channels : int
        Number of input channels (3 for RGB, 1 for grayscale)
    embed_dim : int
        Dimension of the linear projection of the patches,
        which remains constant throughout the model depth

    Attributes
    ----------
    num_patches : int
        Number of patches that will be extracted from the input image
    proj : nn.conv2d
        Not exactly a convolution operation, but to be used for splitting
        the image into patches and
    """

    def __init__(self, img_size, patch_size, in_channels=3, embed_dim=768):
        super().__init__()
        self.img_size = img_size
        self.patch_size = patch_size
        self.num_patches = (img_size**2) // (patch_size ** 2)

        self.proj = nn.Conv2d(in_channels=in_channels, out_channels=
            embed_dim, kernel_size = patch_size, stride = patch_size)

    def forward(self, x):
        """
        Parameters
        ----------
        x : torch.Tensor of shape `(num_samples,in_channels,img_size,img_size)`

        Returns
        -------
        torch.Tensor of shape `(num_samples, num_patches, embed_dim)`
        """
        x = self.proj(x) # after running through the conv layer, we get a 4D
        # tensor (num_samples, embed_dim, √num_patches, √num_patches)
        x = x.flatten(2) # flatten the last two dimensions to get
        # (num_samples, embed_dim, num_patches)
        x = x.transpose(1, 2) # transpose the last two dimensions to get
        # (num_samples, num_patches, embed_dim)    
        return x

class Attention(nn.Module):
    """Attention mechanism for the ViT model

    Parameters
    ----------
    dim : int
        Dimension of the input and output tokens
    num_heads : int
        Number of heads to be used in the multi-head attention mechanism
    qkv_bias : bool
        If true then add a learnable bias to query, key, and value projections
    attn_p : float
        Dropout probability applied to the query, key, and value tensors
    proj_p : float
        Dropout probability applied to the output tensor

    Attributes
    ----------
    scale : float
        Scaling factor to be used to normalize the scaled dot product attention
    qkv : nn.Linear
        Linear projection of the input tokens
    proj : nn.Linear
        The last layer of the attention mechanism. Linear projection of the
        attention as a concatenated output of all the attention heads and maps
        it to a new space of dimension `dim`
    attn_drop, proj_drop : nn.Dropout
        Dropout layer applied to the attention layers & output
    """

    def __init__(self, dim, num_heads=12, qkv_bias=True, attn_p=0., proj_p=0.):
        super().__init__()
        self.dim = dim
        self.num_heads = num_heads
        self.head_dim = dim // num_heads #so that the dimensions input=output
        self.scale = self.head_dim ** -0.5 #from Attention is all you need
        # the idea is prevent feeding extremely large values to the softmax,
        # which would result in a very small gradient!

        self.qkv = nn.Linear(dim, dim * 3, bias=qkv_bias)
        self.proj = nn.Linear(dim, dim)
        self.attn_drop = nn.Dropout(attn_p)
        self.proj_drop = nn.Dropout(proj_p)

    def forward(self, x):
        """
        Parameters
        ----------
        x : torch.Tensor of shape `(num_samples, num_patches, dim)`

        Returns
        -------
        torch.Tensor of shape `(num_samples, num_patches+1, dim)` for an
        additional token (class label) as the first token.
        """
        num_samples, n_tokens, dim = x.shape
        # print("num_samples, n_tokens, dim : ", num_samples, n_tokens, dim)
        if dim!=self.dim:
            raise ValueError("Input dimension must be = output dimension")

        qkv = self.qkv(x) #the input tensor is converted into attention
        qkv = qkv.reshape(num_samples, n_tokens, 3, self.num_heads,
            self.head_dim) #(num_samples,num_patches+1, 3, num_heads, head_dim)
        qkv = qkv.permute(2, 0, 3, 1, 4) # (3, num_samples, num_heads,
        # num_patches, head_dim) change their order for easy extraction next:
        q, k, v = qkv[0], qkv[1], qkv[2] # (num_samples, num_heads, num_patches,
        # head_dim)
        k_t = k.transpose(-2, -1) #(num_samples, num_heads, head_dim,
        # num_patches+1), for the dot product calculation next:
        dp = (q @ k_t) * self.scale # (num_samples, num_heads, num_patches+1,
        # num_patches+1)
        attn = dp.softmax(dim=-1)
        attn = self.attn_drop(attn)
        weighted_avg = attn @ v #(num_samples,num_heads,num_patches+1,head_dim)
        weighted_avg = weighted_avg.transpose(1, 2) # (num_samples,
        # num_patches+1, num_heads, head_dim)
        weighted_avg = weighted_avg.flatten(2) #(num_samples, num_patches+1,
        # dim) Last to dimensions are concatenated
        x = self.proj(weighted_avg) # (num_samples, num_patches+1, dim)
        x = self.proj_drop(x) # (num_samples, num_patches+1, dim)
        # print(x.shape)
        return x

class MLP(nn.Module):
    """Multi-layer perceptron for the ViT model

    Parameters
    ----------
    in_features : int
        Dimension of the input tokens or number of input features
    hidden_features : int
        Dimension of (number of nodes in) the hidden layer
    out_features : int
        Dimension of the output tokens or number of output features
    p : float
        Dropout probability applied to the output tensor

    Attributes
    ----------
    fc1, fc2 : nn.Linear
        Linear layers of the MLP
    act : nn.GELU
        GELU (Gaussian error linear unit) activation function
    drop : nn.Dropout
        Dropout layer applied to the output
    """

    def __init__(self, in_features, hidden_features=None, out_features=None,
                p=0.):
        super().__init__()
        self.fc1 = nn.Linear(in_features, hidden_features)
        self.fc2 = nn.Linear(hidden_features, out_features)
        self.act = nn.GELU()
        self.drop = nn.Dropout(p)

    def forward(self, x):
        """
        Parameters
        ----------
        x : torch.Tensor of shape `(num_samples, num_patches+1, dim)`

        Returns
        -------
        torch.Tensor of shape `(num_samples, num_patches+1, dim)`
        """
        x = self.fc1(x)
        x = self.act(x)
        x = self.drop(x)
        x = self.fc2(x)
        x = self.drop(x) # (num_samples, num_patches+1, dim)

        return x

class Block(nn.Module):
    """A single block of the ViT model

    Parameters
    ----------
    dim : int
        Dimension of the input and output tokens (embedding dimension)
    num_heads : int
        Number of heads to be used in the multi-head attention mechanism
    mlp_ratio : float
        Ratio of the hidden dimension to the input dimension (MLP/dim)
    qkv_bias : bool
        If true then add a learnable bias to query, key, and value projections
    attn_p : float
        Dropout probability applied to the query, key, and value tensors
    proj_p : float
        Dropout probability applied to the output tensor
    drop_p : float
        Dropout probability applied to the output of the MLP

    Attributes
    ----------
    attn : Attention
        module Attention (Attention mechanism for the block)
    norm1 : nn.LayerNorm
        Layer normalization applied to the output of the attention mechanism
    mlp : MLP
        module MLP (Multi-layer perceptron for the block)
    norm2 : nn.LayerNorm
        Layer normalization applied to the output of the MLP
    """

    def __init__(self, dim, num_heads, mlp_ratio=4., qkv_bias=True, attn_p=0.,
                proj_p=0., drop_p=0.):
        super().__init__()
        self.norm1 = nn.LayerNorm(dim, eps=1e-6)
        self.attn = Attention(dim, num_heads, qkv_bias, attn_p, proj_p)
        self.norm2 = nn.LayerNorm(dim, eps=1e-6)
        hidden_features = int(dim * mlp_ratio)
        self.mlp = MLP(in_features=dim, hidden_features=hidden_features,
            out_features=dim, p=drop_p)

    def forward(self, x):
        """
        Parameters
        ----------
        x : torch.Tensor of shape `(num_samples, num_patches+1, dim)`

        Returns
        -------
        torch.Tensor of shape `(num_samples, num_patches+1, dim)`
        """
        # print("x.shape", x.shape)
        x = x + self.attn(self.norm1(x))
        # print("shape after combining with layer nomalized attention", x.shape)
        x = x + self.mlp(self.norm2(x))
        # print("shape after combining with layer nomalized mlp", x.shape)
        return x

class VisionTransformer(nn.Module):
    """Vision Transformer (ViT) model

    Parameters
    ----------
    img_size : int
        Size of the input image (assumed to be square)
    patch_size : int
        Size of the patches to be extracted from the input image (square)
    in_channels : int
        Number of channels in the input image (3 for RGB)
    num_classes : int
        Number of classes in the classification task
    dim : int
        Dimension of the input and output tokens (embedding dimension)
    depth : int
        Number of Transformer blocks to be stacked
    num_heads : int
        Number of heads to be used in the multi-head attention mechanism
    mlp_ratio : float
        Ratio of the hidden dimension to the input dimension (MLP/dim)
    qkv_bias : bool
        If true then add a learnable bias to query, key, and value projections
    attn_p : float
        Dropout probability applied to the query, key, and value tensors
    proj_p : float
        Dropout probability applied to the output tensor
    drop_p : float
        Dropout probability applied to the output of the MLP

    Attributes
    ----------
    patch_embed : PatchEmbed
        module PatchEmbed (Extracts patches from the input image)
    cls_token : nn.Parameter
        Learnable parameter for the class token (first token). It has
        `embed_dim` dimensions
    pos_emb : nn.Parameter
        Learnable parameter for the positional embedding. It has
        `(num_patches+1) x embed_dim` dimensions
    pos_drop : nn.Dropout
    blocks : nn.ModuleList
        Stacked list of blocks of the ViT model
    norm : nn.LayerNorm
        Layer normalization applied to the output of the last block
    head : nn.Linear
        Linear layer applied to the output of the last block
    """

    def __init__(self, img_size=384, patch_size=16, in_channels=3,
                num_classes=1000, embed_dim=768, depth=12, num_heads=12,
                mlp_ratio=4., qkv_bias=True, attn_p=0., proj_p=0., drop_p=0.):
        super().__init__()
        self.patch_embed = PatchEmbed(img_size=img_size,
            patch_size=patch_size, in_channels=in_channels,
                                          embed_dim=embed_dim)
        num_patches = self.patch_embed.num_patches
        self.cls_token = nn.Parameter(torch.zeros(1, 1, embed_dim))
        self.pos_embed = nn.Parameter(torch.zeros(1, 1 +
            self.patch_embed.num_patches, embed_dim))
        self.pos_drop = nn.Dropout(p=drop_p)
        self.blocks = nn.ModuleList([
            Block(dim=embed_dim, num_heads=num_heads, mlp_ratio=mlp_ratio,
                qkv_bias=qkv_bias, attn_p=attn_p, proj_p=proj_p)#, p=p)
            for _ in range(depth)])
        self.norm = nn.LayerNorm(embed_dim, eps=1e-6)
        self.head = nn.Linear(embed_dim, num_classes)

    def forward(self, x):
        """
        Parameters
        ----------
        x : torch.Tensor of shape `(num_samples, in_channels, img_size, img_size)`

        Returns
        -------
        torch.Tensor of shape `(num_samples, num_classes)`
        """
        num_samples = x.shape[0]
        x = self.patch_embed(x)
        cls_tokens = self.cls_token.expand(num_samples, -1, -1)
        x = torch.cat((cls_tokens, x), dim=1)
        x = x + self.pos_embed
        x = self.pos_drop(x)
        for block in self.blocks:
            x = block(x)
        x = self.norm(x)
        cls_token_final = x[:, 0]
        x = self.head(cls_token_final)

        return x
