import numpy as np
from PIL import Image
import torch

k=10
imagenet_labels=dict(enumerate(open('classes.txt')))

model=torch.load('model_custom.pth')
model.eval()

img=(np.array(Image.open('cat.png'))/128)-1
img=torch.from_numpy(img).permute(2,0,1).unsqueeze(0).to(torch.float32)
logits=model(img)
probs=torch.nn.functional.softmax(logits,dim=-1)

top_probs,top_labels=probs[0].topk(k) #torch.topk(probs,k)
for i, (indices_, prob_) in enumerate(zip(top_labels, top_probs)):
    index=indices_.item()
    prob=prob_.item()
    cls=imagenet_labels[index].strip()
    print(f"{i}: {cls:<45} --- ({prob:.4f})")
