# How to create a class:
class Item:
    pass

item1 = Item() # create an instance of a class

# Assign attributes:
item1.name = 'Phone'
item1.price = 100
item1.quantity = 5
#item1.price_total = item1.price * item1.quantity

print(type(item1))
print(type(item1.name)) # str
print(type(item1.price)) # int 
print(type(item1.quantity)) # int 
#print (type (item1.price_total)) # int
