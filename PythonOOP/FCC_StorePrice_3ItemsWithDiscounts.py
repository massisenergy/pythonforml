class Item:
    pay_rate = 0.8 #giving 20% discount for every item sold.
    def __init__(self, name: str, price: float, quantity=0):
        assert price >= 0, f"{price} is negative!"
        assert quantity >= 0, f"{quantity} is not greater than zero!"
        self.name = name
        self.price = price
        self.quantity = quantity
    def calculate_total_price(self):
        return self.price * self.quantity
    def apply_discount(self):
        self.price = self.price * Item.pay_rate # if you want to access from CA (fixed)
        self.price = self.price * self.pay_rate # if you want to access from IA (instance specific)
    
item1 = Item("Phone", 100, 20) #default value '0', so it's not mandatory to pass quantity
item1.apply_discount()
print(item1.name, "total costs after discount:", item1.calculate_total_price())

item2 = Item("Laptop", 1000, 2)
item2.pay_rate = 0.75
item2.apply_discount()
print(item2.name, "total costs after discount:", item2.calculate_total_price())
