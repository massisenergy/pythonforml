class Item:
    def __init__(self, name: str, price: float, quantity=0):
#Use ASSERT statement to impose rules on the arguments:
        assert price >= 0, f"{price} is negative!"
        assert quantity >= 0, f"{quantity} is not greater than zero!"
# Rule that makes sure that specific attributes are assigned!
        self.name = name
        self.price = price
        self.quantity = quantity
    def calculate_total_price(self):
        return self.price * self.quantity
    
item1 = Item("Phone", 100) #default value '0', so it's not mandatory to pass quantity
item2 = Item("Laptop", 1000, -1)
item2.has_numpad = False
print(item1.calculate_total_price())
print(item2.calculate_total_price())
