import csv
class Item:
    pay_rate = 0.8 #giving 20% discount for every item sold.
    all = []
    def __init__(self, name: str, price: float, quantity=0):
        assert price >= 0, f"{price} is negative!"
        assert quantity >= 0, f"{quantity} is not greater than zero!"
        self.name = name
        self.price = price
        self.quantity = quantity
        Item.all.append(self) #Actions to execute
        
    def calculate_total_price(self):
        return self.price * self.quantity
    def apply_discount(self):
        self.price = self.price * Item.pay_rate # if you want to access from CA (fixed)
        self.price = self.price * self.pay_rate # to access from IA (instance specific)
    
    @classmethod
    def instantiate_from_csv(cls): #doesn't receive the instances as 'self' but 'cls'
        with open('item.csv', 'r') as file:
            reader = csv.DictReader(file)
            items = list(reader)
            
        for item in items:
            print(item)
            Item(
                name=item.get('name'),
                price=float(item.get('price')), #otherwise will take it as `string`
                quantity=int(item.get('quantity')),
            )

    def __repr__(self):
        return f"Item('{self.name}', '{self.price}', '{self.quantity}')"
    
Item.instantiate_from_csv()
print(Item.all)
