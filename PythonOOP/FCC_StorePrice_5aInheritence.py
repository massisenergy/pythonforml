import csv

class Item:
    pay_rate = 0.8 #giving 20% discount for every item sold.
    all = []
    def __init__(self, name: str, price: float, quantity=0):
        assert price >= 0, f"{price} is not greater than zero!"
        assert quantity >= 0, f"{quantity} is not not greater than zero!"
        self.name = name
        self.price = price
        self.quantity = quantity
        Item.all.append(self)
    def calculate_total_price(self):
        return self.price * self.quantity  
    def apply_discount(self):
        self.price = self.price * self.pay_rate

    @classmethod
    def instantiate_from_csv(cls): #note: 'cls' not 'self'
        with open('item.csv', 'r') as f:
            readr = csv.DictReader(f)
            items = list(readr) 
        for item in items: #print(item)
            Item(
                name=item.get('name'),
                price=float(item.get('price')),
                quantity=int(item.get('quantity')),
            )

    @staticmethod
    def is_integer(num): #never send the instance as the first argument, unlike CM.
        #We will count the floats that are point zero
        if isinstance(num, float):
            return num.is_integer()
        elif isinstance(num, int):
            return True
        else:
            return False
            
    def __repr__(self):
        return f"item('{self.name}', {self.price}, {self.quantity})"

#Introduce INHERITENCE
class Phone(Item): #Inherits from the superclass 'Item'
    all = []
    def __init__(self, name: str, price: float, quantity=0, broken_phones=0):
        assert price >= 0, f"{price} is not greater than zero!"
        assert quantity >= 0, f"{quantity} is not not greater than zero!"
        self.name = name
        self.price = price
        self.quantity = quantity
        self.broken_phones = broken_phones
        Phone.all.append(self)
    

phone1 = Phone("jioPhonev10", 500, 5) #changed 'Item()' to 'Phone()'
phone2 = Phone("jioPhonev20", 700, 5)
print(phone1.calculate_total_price())
