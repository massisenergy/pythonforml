import csv

class Item:
    pay_rate = 0.8 #giving 20% discount for every item sold.
    all = []
    def __init__(self, name: str, price: float, quantity=0):
        assert price >= 0, f"{price} is not greater than zero!"
        assert quantity >= 0, f"{quantity} is not not greater than zero!"
        self.name = name
        self.price = price
        self.quantity = quantity
        Item.all.append(self)
    def calculate_total_price(self):
        return self.price * self.quantity  
    def apply_discount(self):
        self.price = self.price * self.pay_rate

    @classmethod
    def instantiate_from_csv(cls): #note: 'cls' not 'self'
        with open('item.csv', 'r') as f:
            readr = csv.DictReader(f)
            items = list(readr) 
        for item in items: #print(item)
            Item(
                name=item.get('name'),
                price=float(item.get('price')),
                quantity=int(item.get('quantity')),
            )

    @staticmethod
    def is_integer(num): #never send the instance as the first argument, unlike CM.
        if isinstance(num, float):
            return num.is_integer()
        elif isinstance(num, int):
            return True
        else:
            return False
            
    def __repr__(self):
        return f"{self.__class__.__name__}('{self.name}', {self.price}, {self.quantity})"
    
    @property
    def read_only_name(self):
        return "AAA"
