from item_Decorator import Item

item1 = Item("myItem", 750)
item1.name = "otherItem"
print(item1.read_only_name) #this works as it should

item1.read_only_name = "BBB" #this throws error as it should
