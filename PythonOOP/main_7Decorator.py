from item_Decorator2 import Item

item1 = Item("myItem", 750)
print(item1.name) #NOTE: we don't need `_name` here, although it's implemented in that way
item1.name = "OtherItem "#this throws error as it should
