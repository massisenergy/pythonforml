from item_Decorator3 import Item

item1 = Item("myItem", 750)
item1.name = "OtherItem"
print(item1.name) #NOTE: we don't need `__name` here, although it's implemented in that way
item1.name = "OtherItem_BLABLA"
print(item1.name)
