from item import Item

#Introduce INHERITENCE
class Phone(Item): #Inherits from the class 'Item'
    def __init__(self, name: str, price: float, quantity=0, broken_phones=0):
        super().__init__(name, price, quantity)
        assert quantity >= 0, f"{quantity} is not not greater than zero!"
        self.broken_phones = broken_phones
